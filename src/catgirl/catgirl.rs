use amethyst::{
    ecs::{Component, DenseVecStorage, Entity}
};

use rand::{
    distributions::{Distribution, Standard},
    Rng,
}; // 0.8.0

use std::fmt::{self, Debug, Display};

#[derive(Clone)]
pub struct Catgirl {
    pub width: f32,
    pub height: f32,

    pub speed: i32,
    pub direc: [f32; 2],
    pub target: [f32; 2],

    pub boredom: f32,
    pub age: i32,
    pub name: String,
    pub mood: Mood,
    pub need: [Option<Need>; 10]
}

// this is our catgirl interface, and will determine what our catgirl can do. 
// i.e. scratch_ears, increase_love, change_mood etc...
impl Catgirl {
    pub fn new(w: f32, h: f32) -> Catgirl {
        Catgirl {
            width: w,
            height: h,

            speed: 0,
            direc: [0.0, 0.0],
            target: [0.0, 0.0],

            boredom: 0.0,

            age: 12,
            name: String::from("Hitomi"),
            mood: Mood::Happy,
            need: [None; 10]
        }
    }

    pub fn change_mood(&mut self) {
        self.mood = rand::random();
    }

}

pub struct EmotionText {
    pub emotion_text: Entity,
}

impl Component for Catgirl {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug, Clone, PartialEq)]
pub enum Mood {
    Happy,
    Upset,
    Angry,
    Inquisitive,
    Feisty,
    Willful,
    Lonely, 
    Destructive,
    Nurturing
}

impl Display for Mood {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
        // or, alternatively:
        // fmt::Debug::fmt(self, f)
    }
}

impl Distribution<Mood> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Mood {
        match rng.gen_range(0, 8) {
            0 => Mood::Happy,
            1 => Mood::Upset,
            2 => Mood::Angry,
            3 => Mood::Inquisitive,
            4 => Mood::Feisty,
            5 => Mood::Willful,
            6 => Mood::Lonely,
            7 => Mood::Destructive,
            8 => Mood::Nurturing,
            _ => Mood::Happy
        }
    }
}

impl Distribution<Need> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Need {
        match rng.gen_range(0, 8) {
            0 => Need::Sunlight,
            1 => Need::Food,
            2 => Need::Pats,
            3 => Need::Hunt,
            4 => Need::Love,
            5 => Need::Chaos,
            _ => Need::Sunlight
        }
    }
}

#[derive(Clone)]
pub enum Need {
    Sunlight,
    Food,
    Pats,
    Hunt,
    Love,
    Chaos
}

impl Copy for Need {
    
}