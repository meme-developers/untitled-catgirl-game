

extern crate amethyst;

use amethyst::{
    assets::{AssetStorage, Loader, Handle},
    core::transform::Transform,
    core::timing::Time,
    prelude::*,
    utils::application_root_dir,
    renderer::{Camera, ImageFormat, SpriteRender, SpriteSheet, SpriteSheetFormat, Texture},
    ui::{Anchor, LineMode, TtfFormat, UiText, UiTransform},
};

use std::collections::HashMap;
use std::fs;

use crate::catgirl::{
    Paddle, FloorPlan, Ball, Side, Catgirl, Player, Interactible, EmotionText,
    ARENA_HEIGHT, ARENA_WIDTH, PADDLE_WIDTH, BALL_RADIUS, BALL_VELOCITY_X, BALL_VELOCITY_Y
};

#[derive(Default)]
pub struct Game {
    ball_spawn_timer: Option<f32>,
    sprite_sheet_handle: Option<Handle<SpriteSheet>>,
    interactibles_sheet_handle: Option<Handle<SpriteSheet>>,
    background_handle: Option<Handle<SpriteSheet>>,
    interactibles_map: HashMap<String, SpriteRender>
}

impl SimpleState for Game {
    // oninit is called during the plumbing stage
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;
        self.interactibles_map = HashMap::new();
        
        // Wait five second before spawning the ball.
        self.ball_spawn_timer.replace(5.0);
        self.sprite_sheet_handle.replace(load_actors(world));
        self.background_handle.replace(load_background(world));
        self.interactibles_sheet_handle.replace(load_interactibles(world));

        world.register::<FloorPlan>();

        initialise_background(world, self.background_handle.clone().unwrap());
        initialise_actors(world, self.sprite_sheet_handle.clone().unwrap());
        initialise_catgirl(world, self.sprite_sheet_handle.clone().unwrap());
        initialise_player(world, self.sprite_sheet_handle.clone().unwrap());
        self.seed_interactibles_map(self.interactibles_sheet_handle.clone().unwrap());
        self.populate_world(world);
        initialise_camera(world);
        initialise_scoreboard(world);
    }

    // our execute function in the game loop
    fn update(&mut self, data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        if let Some(mut timer) = self.ball_spawn_timer.take() {
            // If the timer isn't expired yet, subtract the time that passed since the last update.
            {
                let time = data.world.fetch::<Time>();
                timer -= time.delta_seconds();
            }
            if timer <= 0.0 {
                // When timer expire, spawn the ball and give the catgirl a new emotion
                initialise_ball(data.world, self.sprite_sheet_handle.clone().unwrap());

            } else {
                // If timer is not expired yet, put it back onto the state.
                self.ball_spawn_timer.replace(timer);
            }
        }
        Trans::None
    }
}

impl Game {
    // populates the world
    fn populate_world(&mut self, world: &mut World) {

        let app_root = application_root_dir().unwrap();
        let interactibles_path = app_root.join("assets").join("interactibles").join("interactibles.ron");
        let contents = fs::read_to_string(interactibles_path)
            .expect("Something went wrong reading the file");
        let res = ron::de::from_str::<Vec<Interactible>>(&contents).unwrap();

        for n in res {
            let render = self.interactibles_map.get(&n.name).unwrap();
            let mut transform = Transform::default();
            transform.set_translation_xyz(n.x, n.y, 0.0);
            world.create_entity()
                .with(render.clone())
                .with(transform)
                .build();
        }
    }

    // puts the interactibles on the screen
    fn seed_interactibles_map(&mut self, sprite_sheet_handle: Handle<SpriteSheet>) {
        // Correctly position the fridge.
        self.interactibles_map.insert(
            String::from("fridge"), 
            SpriteRender::new(sprite_sheet_handle.clone(), 0)
        );

        // Correctly position the sink.
        self.interactibles_map.insert(
            String::from("sink"), 
            SpriteRender::new(sprite_sheet_handle.clone(), 1)
        );

        // Correctly position the couch.
        self.interactibles_map.insert(
            String::from("couch"), 
            SpriteRender::new(sprite_sheet_handle.clone(), 2)
        );

        // Correctly position the donut.  
        self.interactibles_map.insert(
            String::from("donut"), 
            SpriteRender::new(sprite_sheet_handle, 3)
        );
    }
}

// defines the render target
fn initialise_camera(world: &mut World) {
    // Setup camera in a way that our screen covers whole arena and (0, 0) is in the bottom left.
    let mut transform = Transform::default();
    transform.set_translation_xyz(ARENA_WIDTH * 0.5, ARENA_HEIGHT * 0.5, 1.0);

    world
        .create_entity()
        .with(Camera::standard_2d(ARENA_WIDTH, ARENA_HEIGHT))
        .with(transform)
        .build();
}

/// Initialises one paddle on the left, and one paddle on the right.
fn initialise_actors(world: &mut World, sprite_sheet_handle: Handle<SpriteSheet>) {
    let mut left_transform = Transform::default();
    let mut right_transform = Transform::default();

    // Correctly position the paddles.
    let y = ARENA_HEIGHT / 2.0;
    left_transform.set_translation_xyz(PADDLE_WIDTH * 0.5, y, 0.0);
    right_transform.set_translation_xyz(ARENA_WIDTH - (PADDLE_WIDTH * 0.5), y, 0.0);

    let paddle_render = SpriteRender::new(sprite_sheet_handle.clone(), 0);  // paddle is the first sprite in the sprite_sheet
    
    // Create a left plank entity.
    world
        .create_entity()
        .with(paddle_render.clone())
        .with(Paddle::new(Side::Left))
        .with(left_transform)
        .build();

    // Create right plank entity.
    world
        .create_entity()
        .with(paddle_render.clone())
        .with(Paddle::new(Side::Right))
        .with(right_transform)
        .build();


}

// puts the catgirl in the world
fn initialise_catgirl(world: &mut World, sprite_sheet_handle: Handle<SpriteSheet>) {
    // give her a translation so she can move
    let mut catgirl_spawn = Transform::default();
    let y = (ARENA_HEIGHT / 2.0) - 100.0;
    catgirl_spawn.set_translation_xyz(100.0, y, 0.0);

    // define where she is in the texture
    let catgirl_render = SpriteRender::new(sprite_sheet_handle, 2);  // catgirl is the third sprite in the sprite_sheet
    let catgirl = Catgirl::new(36.0, 54.0);

    //put her in the ECS db
    world.create_entity()
        .with(catgirl_render)
        .with(catgirl)
        .with(catgirl_spawn)
        .build();
}

// puts the player in the world
fn initialise_player(world: &mut World, sprite_sheet_handle: Handle<SpriteSheet>) {
    // give her a translation so she can move
    let mut player_spawn = Transform::default();
    let y = (ARENA_HEIGHT / 2.0) - 100.0;
    player_spawn.set_translation_xyz(200.0, y, 0.0);

    // define where she is in the texture
    let player_render = SpriteRender::new(sprite_sheet_handle, 3);  // player is the third sprite in the sprite_sheet

    //put her in the ECS db
    world.create_entity()
        .with(player_render)
        .with(Player::new())
        .with(player_spawn)
        .build();
}

/// Initialises one ball in the middle-ish of the arena.
fn initialise_ball(world: &mut World, sprite_sheet_handle: Handle<SpriteSheet>) {
    // Create the translation.
    let mut local_transform = Transform::default();
    local_transform.set_translation_xyz(ARENA_WIDTH / 2.0, ARENA_HEIGHT / 2.0, 0.0);

    // Assign the sprite for the ball. The ball is the second sprite in the sheet.
    let sprite_render = SpriteRender::new(sprite_sheet_handle, 1);

    world
        .create_entity()
        .with(sprite_render)
        .with(Ball {
            radius: BALL_RADIUS,
            velocity: [BALL_VELOCITY_X, BALL_VELOCITY_Y],
        })
        .with(local_transform)
        .build();
}

// puts the background on the screen
fn initialise_background(world: &mut World, sprite_sheet_handle: Handle<SpriteSheet>) {
    // Correctly position the background.
    let mut background_transform = Transform::default();
    background_transform.set_translation_xyz(ARENA_WIDTH * 0.5, ARENA_HEIGHT * 0.5, -10.0);
    let sprite_render = SpriteRender::new(sprite_sheet_handle, 0); 
    
    // Create floorplan
    world
        .create_entity()
        .with(sprite_render.clone())
        .with(background_transform)
        .build();
}

// loads raw textures for background from disk into memory
fn load_background(world: &mut World) -> Handle<SpriteSheet> {
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "backgrounds/floorplan.png",
            ImageFormat::default(),
            (),
            &texture_storage,
        )
    };

    let loader = world.read_resource::<Loader>();
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
        "backgrounds/floorplan.ron", // Here we load the associated ron file
        SpriteSheetFormat(texture_handle),
        (),
        &sprite_sheet_store,
    )
}

// loads raw textures for interactibles from disk into memory
fn load_interactibles(world: &mut World) -> Handle<SpriteSheet> {
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "interactibles/objects-sprite-sheet.png",
            ImageFormat::default(),
            (),
            &texture_storage,
        )
    };

    let loader = world.read_resource::<Loader>();
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
        "interactibles/objects.ron", // Here we load the associated ron file
        SpriteSheetFormat(texture_handle),
        (),
        &sprite_sheet_store,
    )
}

// loads raw textures from disk into memory
fn load_actors(world: &mut World) -> Handle<SpriteSheet> {
    // Load the sprite sheet necessary to render the graphics.
    // The texture is the pixel data
    // `texture_handle` is a cloneable reference to the texture
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "texture/pong_spritesheet.png",
            ImageFormat::default(),
            (),
            &texture_storage,
        )
    };

    let loader = world.read_resource::<Loader>();
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
        "texture/pong_spritesheet.ron", // Here we load the associated ron file
        SpriteSheetFormat(texture_handle),
        (),
        &sprite_sheet_store,
    )
}

fn initialise_scoreboard(world: &mut World) {
    let font = world.read_resource::<Loader>().load(
        "font/marker_felt.ttf",
        TtfFormat,
        (),
        &world.read_resource(),
    );
    let p1_transform = UiTransform::new(
        "Emotions".to_string(), Anchor::TopRight, Anchor::TopRight,
        -50., -50., 1., 200., 50.,
    );

    let emotion_text = world
        .create_entity()
        .with(p1_transform)
        .with(UiText::new(
            font.clone(),
            "Genki <3".to_string(),
            [0.85, 0.3, 0.76, 1.],
            50.,
            LineMode::Single,
            Anchor::Middle,
        ))
        .build();

    world.insert(EmotionText { emotion_text });
}