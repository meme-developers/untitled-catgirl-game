use amethyst::{
    ecs::{Component, DenseVecStorage}
};

use serde::{Serialize, Deserialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct Interactible {
    pub name: String,
    // position
    pub x: f32,
    pub y: f32,
    // dimensions
    pub w: f32,
    pub h: f32
}

impl Component for Interactible {
    type Storage = DenseVecStorage<Self>;
}