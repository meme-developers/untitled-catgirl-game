pub use self::ball::Ball;
pub use self::paddle::Paddle;
pub use self::side::Side;
pub use self::floor_plan::FloorPlan;
pub use self::catgirl::{ Catgirl, Mood, EmotionText };
pub use self::player::Player;
pub use self::game::Game;
pub use self::interactible::Interactible;

pub const ARENA_HEIGHT: f32 = 659.0;
pub const ARENA_WIDTH: f32 = 1280.0;

pub const PADDLE_HEIGHT: f32 = 16.0;
pub const PADDLE_WIDTH: f32 = 4.0;

pub const BALL_VELOCITY_X: f32 = 75.0;
pub const BALL_VELOCITY_Y: f32 = 50.0;
pub const BALL_RADIUS: f32 = 2.0;

mod game;
mod paddle;
mod catgirl;
mod floor_plan;
mod ball;
mod side;
mod player;
mod interactible;