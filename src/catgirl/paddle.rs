use amethyst::{
    ecs::{Component, DenseVecStorage}
};

use crate::catgirl::{
    Side, PADDLE_HEIGHT, PADDLE_WIDTH
};

pub struct Paddle {
    pub side: Side,
    pub width: f32,
    pub height: f32,
}

impl Paddle {
    pub fn new(side: Side) -> Paddle {
        Paddle {
            side,
            width: PADDLE_WIDTH,
            height: PADDLE_HEIGHT,
        }
    }
}

impl Component for Paddle {
    type Storage = DenseVecStorage<Self>;
}