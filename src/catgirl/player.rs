use amethyst::{
    ecs::{Component, DenseVecStorage}    
};

pub struct Player {
    pub width: f32,
    pub height: f32,

    pub speed: f32,
    pub direc: [i32; 2],
}

impl Player {
    pub fn new() -> Player {
        Player {
            width: 0.0,
            height: 0.0,

            speed: 0.0,
            direc: [0, 0]
        }
    }
}

impl Component for Player {
    type Storage = DenseVecStorage<Self>;
}