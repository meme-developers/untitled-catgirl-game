#[derive(PartialEq, Eq)]
pub enum Side {
    Left,
    Right,
}