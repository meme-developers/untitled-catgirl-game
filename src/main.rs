use amethyst::{
    core::transform::TransformBundle,
    prelude::*,
    renderer::{
        plugins::{RenderFlat2D, RenderToWindow},
        types::DefaultBackend,
        RenderingBundle
    },
    utils::application_root_dir,
    input::{InputBundle, StringBindings},
    ui::{RenderUi, UiBundle}
};

// Import the modules
mod catgirl;
mod systems;

use crate::catgirl::Game;

// the only function in this file is our program entry point
fn main() -> amethyst::Result<()> {
    //log to console... we are using the windows console subsystem but binding the win32 api by using the winit stuff contained in amethyst
    amethyst::start_logger(Default::default());
    // Read configuration from disk
    let app_root = application_root_dir()?;
    // basic window features (height / width)
    let display_config_path = app_root.join("config").join("display.ron");

    // key bindings
    let binding_path = app_root.join("config").join("bindings.ron");
    let input_bundle = InputBundle::<StringBindings>::new()
        .with_bindings_from_file(binding_path)?;

    // GameDataBuilder gives us a global world object
    // configurate it with the Fluent API
    let game_data = GameDataBuilder::default()
        // rendering pipeline
        .with_bundle(
            RenderingBundle::<DefaultBackend>::new()
                // The RenderToWindow plugin provides all the scaffolding for opening a window and drawing on it
                .with_plugin(
                    RenderToWindow::from_config_path(display_config_path)?
                        .with_clear([0.003516, 0.11726, 0.21765, 1.0])
                )
                .with_plugin(RenderUi::default())
                // RenderFlat2D plugin is used to render entities with a `SpriteRender` component.
                .with_plugin(RenderFlat2D::default()),
        )?
        // move and resize
        .with_bundle(TransformBundle::new())?
        // input pipeline - messagepump handler
        .with_bundle(input_bundle)?
        // rendering text to screen
        .with_bundle(UiBundle::<StringBindings>::new())?
        // import our first-class citizens where our code lives
        .with(systems::PaddleSystem, "paddle_system", &["input_system"])
        .with(systems::MoveBallsSystem, "move_balls", &[])
        .with(systems::EmotionSystem, "emotions_system", &[])
        .with(systems::PlayerMoveSystem, "player_move_system", &["input_system"])
        .with(
            systems::BounceSystem,
            "collision_system",
            &["paddle_system", "move_balls"],
        );

    // let world know where our assets live
    let assets_dir = app_root.join("assets");
    let mut game = Application::new(assets_dir, Game::default(), game_data)?;
    // run game loop. From here, game will decide when to hook in to any of our code's run methods
    game.run();

    Ok(())
}