use amethyst::{
    core::{Transform},
    core::timing::Time,
    derive::SystemDesc,
    ui::UiText,
    ecs::{Join, WriteStorage, Read, ReadExpect, System, SystemData}
};

use rand::random;
use vecmath::{vec2_normalized, vec2_sub};

use crate::catgirl::{
    Catgirl, Mood, EmotionText,
    ARENA_HEIGHT, ARENA_WIDTH
};

#[derive(SystemDesc)]
pub struct EmotionSystem;

impl<'s> System<'s> for EmotionSystem {
    type SystemData = (
        WriteStorage<'s, Catgirl>,
        WriteStorage<'s, UiText>,
        WriteStorage<'s, Transform>,
        Read<'s, Time>,
        ReadExpect<'s, EmotionText>
    );

    fn run(&mut self, (mut catgirls, mut ui_text, mut transforms, time, emotion_text): Self::SystemData) {
        for (catgirl, transform) in (&mut catgirls, &mut transforms).join() {

            // The catgirl's boredom increases as time passes
            catgirl.boredom += time.delta_seconds();
            let old_catgirl = catgirl.clone();

            // Once a certain number of seconds (5) passes, the catgirl should change target
            if catgirl.boredom > 5.0 {
                catgirl.change_mood();
                catgirl.boredom = 0.0;

                catgirl.target = self.bounds_check([
                    (random::<f32>() * ARENA_WIDTH).floor(),
                    (random::<f32>() * ARENA_HEIGHT).floor()
                ], &catgirl);
            }

            // the catgirl should then get a velocity that will make her move towards target
            if catgirl.target != old_catgirl.target {
                if let Some(text) = ui_text.get_mut(emotion_text.emotion_text) {
                    text.text = catgirl.mood.to_string();
                }
                catgirl.speed = match &catgirl.mood {
                    Mood::Happy => 4,
                    Mood::Upset => 0,
                    Mood::Willful => 7,
                    Mood::Angry => 2,
                    Mood::Destructive => 8,
                    Mood::Feisty => 3,
                    Mood::Inquisitive => 1,
                    Mood::Lonely => 1,
                    Mood::Nurturing => 0
                };
            }

            // move the catgirl... for this we need her entity's translation
            let catgirl_loc = [transform.translation().x, transform.translation().y];

            if catgirl_loc != catgirl.target {
                // Get the direction she should be facing
                catgirl.direc = vec2_normalized(
                    vec2_sub(catgirl.target, catgirl_loc)  
                );

                // now figure out how far she is
                let diff = vec2_sub(catgirl.target, catgirl_loc);

                let sp32 = catgirl.speed as f32;
                if diff[0].abs() < sp32 && diff[1].abs() < sp32 {
                    // teleport her there if she's close enough
                    transform.set_translation_xyz(catgirl.target[0], catgirl.target[1], 0.0);
                } else {
                    // or move her at speed
                    transform.prepend_translation_x(catgirl.direc[0] * sp32);
                    transform.prepend_translation_y(catgirl.direc[1] * sp32);
                }
            }

            // printing effect
            if catgirl.mood != old_catgirl.mood {
                println!("boredom: {}, Mood: {:?}, target: {:?}", catgirl.boredom, catgirl.mood, catgirl.target);
            }
        }
    }
}

impl EmotionSystem {
    fn bounds_check(&mut self, mut input: [f32;2], catgirl: &Catgirl) -> [f32;2] {
        let x = catgirl.width / 2.0;
        let y = catgirl.height / 2.0;
        
        input[0] = input[0].max(x);
        input[0] = input[0].min(ARENA_WIDTH - x);
        input[1] = input[1].max(y);
        input[1] = input[1].min(ARENA_HEIGHT - y);

        input
    }


}