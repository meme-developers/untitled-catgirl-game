use amethyst::core::{Transform};
use amethyst::derive::SystemDesc;
use amethyst::ecs::{Join, Read, ReadStorage, System, SystemData, WriteStorage};
use amethyst::input::{InputHandler, StringBindings};

use crate::catgirl::{Player, ARENA_HEIGHT, ARENA_WIDTH, PADDLE_HEIGHT};

#[derive(SystemDesc)]
pub struct PlayerMoveSystem;

impl<'s> System<'s> for PlayerMoveSystem {
    type SystemData = (
        WriteStorage<'s, Transform>,
        ReadStorage<'s, Player>,
        Read<'s, InputHandler<StringBindings>>,
    );

    fn run(&mut self, (mut transforms, players, input): Self::SystemData) {
        for (_player, transform) in (&players, &mut transforms).join() {
            let movement_y = input.axis_value("player_vert");
            let movement_x = input.axis_value("player_horiz");
            
            if let Some(mv_amount) = movement_y {
                let scaled_amount = 1.4 * mv_amount as f32;    
                let player_y = transform.translation().y;
                transform.set_translation_y(
                    (player_y + scaled_amount)
                        .min(ARENA_HEIGHT - PADDLE_HEIGHT * 0.5)
                        .max(PADDLE_HEIGHT * 0.5)
                );
            }
            
            if let Some(mv_amount) = movement_x {
                let scaled_amount = 1.4 * mv_amount as f32;
                let player_x = transform.translation().x;
                transform.set_translation_x(
                    (player_x + scaled_amount)
                        .min(ARENA_WIDTH - PADDLE_HEIGHT * 0.5)
                        .max(PADDLE_HEIGHT * 0.5)
                );
            }
        }
    }
}